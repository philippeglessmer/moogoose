/**
 * Importation de mongoose
 */
const mongoose = require("mongoose");
const { Schema } = mongoose;

/**
 * Création du schéma User
 */
const userSchema = new Schema({
  email: { type: String, unique: true, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  age: { type: Number, min: 0 },
});
/**
 * Exportation du modèle User
 */
module.exports = mongoose.model("User", userSchema);
