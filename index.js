const mongoos = require('mongoose');
const User = require('./models/User');
const Post = require('./models/Post');
const { default: mongoose } = require('mongoose');

async function main(){
    await mongoos.connect('mongodb://localhost:27017/exemple-mongoose');  
    console.log('Connexion bdd Ok');
    // const user = new User({
    //     email: 'glessmer@live.fr',
    //     firstName: 'Philippe',
    //     lastName: 'GLESSMER',
    //     age: 43
    // });
    // console.log(user);
    // await user.save();

     
    // await User.create({
    //     email: 'philippe.glessmer@akkodis.com',
    //     firstName: 'Philippe',
    //     lastName: 'GLESSMERDEUX',
    //     age: 43
    // });

    // MAJ
    // const users = await User.findById("66091429bd5bd491cb356fd1");
    // users.lastName = "Dupont";
    // await users.save();
    // const id = "66091429bd5bd491cb356fd1";
    // const user = await User.findByIdAndUpdate(id, {
    //     lastName: "Dupont test"
    // },
    // {
    //     new: true
    // });

    // console.log(user);

    // MAJ multiple
    // const res = await User.updateMany(
    //     { email: "glessmer@live.fr" },
    //     { lastName: "Dupont tests" }
    // );
    // console.log(res);

    // Delete
    // const user = await User.findById("66091429bd5bd491cb356fd1");
    // console.log(user);
    // await user.remove();
    // await User.deleteOne({ _id: "66091429bd5bd491cb356fd1" });

    // const philippe1 = await User.findById("660915453a372aaf7902f5fa");
    // const philippe2 = await User.findById("660921b4e1494418cc2be804");
    
    // // Create Post
    // await Post.create({
    //     title: "Mon premier post",
    //     content: "Contenu de mon premier post",
    //     status: "published",
    //     author: philippe1._id
    // });
    // await Post.create({
    //     title: "Mon deuxième post",
    //     content: "Contenu de mon deuxième post",
    //     status: "published",
    //     author: philippe1._id
    // });
 
    mongoose.disconnect();
}

main();